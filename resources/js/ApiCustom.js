import axios from "axios";
import constants from "./constants";
import router from "./routes";
import store from "./store";

const api = axios.create();
api.interceptors.request.use(
    (config) => {
        if (store.state.accessToken) {
            config.headers.authorization = `Bearer ${store.state.accessToken}`;
        }
        return config;
    },
    (error) => {}
);

api.interceptors.response.use(
    (config) => {
        if (store.state.accessToken) {
            config.headers.authorization = `Bearer ${store.state.accessToken}`;
        }

        return config;
    },
    (error) => {
        console.log(error.response);

        if (error.response.data.message === `Token has expired`) {
            console.log(1111111111);
            axios
                .post(
                    "/api/auth/refresh",
                    {},
                    {
                        headers: {
                            authorization: `Bearer ${store.state.accessToken}`,
                        },
                    }
                )
                .then((res) => {
                    store.commit("SET_AUTH_TOKEN", res.data);
                    store.commit("SET_AUTHENTICATED", true);
                    error.config.headers.authorization = `Bearer ${store.state.accessToken}`;

                    return api.request(error.config);
                })
                .catch((error) => {
                    console.log("err", error.response);
                });
        }
        if (error.response.status === 500) {
            console.log(2222);

            store.commit("SET_AUTHENTICATED", false);
            store.commit("REMOVE_AUTH_TOKEN");
            window.location.replace("login");
            // router.push({ name: "login" });
        }
    }
);

export default api;
