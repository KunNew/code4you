require("./bootstrap");

window.Vue = require("vue").default;

const { default: router } = require("./routes");
import Vuetify from "./plugins/vuetify";

import store from "./store";

const files = require.context("./", true, /\.vue$/i);
files
    .keys()
    .map((key) =>
        Vue.component(key.split("/").pop().split(".")[0], files(key).default)
    );

const app = new Vue({
    store,
    vuetify: Vuetify,
    router,
    el: "#app",
});
