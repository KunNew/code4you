import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import createPersistedState from "vuex-persistedstate";
import axiosApiInstance from "../utilites";
import router from "../routes";
import constants from "../constants";
import Api from "../ApiCustom";

Vue.use(Vuex);

const store = new Vuex.Store({
    plugins: [createPersistedState()],
    state: {
        authenticated: false,
        user: {
            id: null,
            first_name: null,
            last_name: null,
            username: null,
            email: null,
            menus: [],
            ability: [],
        },
        accessToken: null,
        refreshToken: null,
        isTokenRefreshing: false,
    },
    getters: {
        authenticated(state) {
            return state.authenticated;
        },
        user(state) {
            return state.user;
        },
        accessToken(state) {
            return state.accessToken;
        },
    },
    mutations: {
        SET_AUTH_USER(state, value) {
            state.user = value;
        },
        SET_AUTH_TOKEN(state, token) {
            state.accessToken = token.access_token;
            // state.refreshToken = token.refresh_token;
        },

        SET_AUTHENTICATED(state, value) {
            state.authenticated = value;
        },
        SET_REFRESHING(state, value) {
            state.isTokenRefreshing = value;
        },
        REMOVE_AUTH_TOKEN(state) {
            state.accessToken = null;
            state.refreshToken = null;
        },
    },
    actions: {
        getUser({ commit }) {
            return new Promise((resolve, reject) => {
                // axiosApiInstance
                //     .post("/auth/me")
                //     .then((response) => {
                //         commit("SET_AUTH_USER", response.data);
                //         console.log("info", response.data);
                //         resolve();
                //     })
                //     .catch((error) => {
                //         reject();
                //         console.log("error", error.response);
                //     });
                Api.post("/api/auth/me")
                    .then((res) => {
                        console.log(res.data);
                    })
                    .catch((error) => {
                        console.log(error.response.data.message);
                    });
            });
        },

        login({ commit }, credential) {
            return new Promise((resolve, reject) => {
                axios
                    .post("api/auth/login", credential)
                    .then((response) => {
                        commit("SET_AUTHENTICATED", true);
                        commit("SET_AUTH_TOKEN", response.data);
                        resolve(response);
                    })
                    .catch((response) => {
                        reject(response);
                    });
            });
        },
        logout({ commit }) {
            commit("SET_AUTHENTICATED", false);
            commit("REMOVE_AUTH_TOKEN");
            router.push({ name: "login" });
        },

        refreshToken({ commit, state }) {
            return new Promise((resolve, reject) => {
                commit("SET_REFRESHING", true);
                axios
                    .post(
                        "api/auth/refresh",
                        {
                            // access_token: state.accessToken,
                        },
                        {}
                    )
                    .then((response) => {
                        commit("SET_AUTHENTICATED", true);
                        commit("SET_AUTH_TOKEN", response.data);
                        resolve();
                    })
                    .catch((response) => {
                        commit("SET_AUTHENTICATED", false);
                        commit("REMOVE_AUTH_TOKEN");
                        window.localStorage.setItem(
                            "redirect",
                            window.location
                                .toString()
                                .replace(constants.baseUrl, "")
                        );
                        router.push({ name: "login" });
                        reject(response);
                    })
                    .finally(() => {
                        commit("SET_REFRESHING", false);
                    });
            });
        },
    },
});

export default store;
